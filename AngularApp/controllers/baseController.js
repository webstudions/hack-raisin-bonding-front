
app.controller('baseController', function($scope, $http){

    $scope.title = 'Raisin';


    $scope.init = function(){

    }

    
    $scope.ajaxGet = function(url, data, callback, errorCallback){
        $scope.ajaxCounter++;
        var req = {
            method: 'GET',
            url: urlService.compose(url),
            params: data
        }
        $http(req).then(function(response){ 
            $scope.ajaxCounter--;
            callback(response.data) 
        }, function(err){ 
            $scope.ajaxCounter--;
            if(errorCallback){
                errorCallback(err)
                return;
            }
            else
                Materialize.toast('ERROR: ' + err.status + ' , Message: ' + err.statusText, 4000);
        });
    }

    $scope.ajaxPost = function(url, data, callback, errorCallback){
        $scope.ajaxCounter++;
        var req = {
            method: 'POST',
            url: urlService.compose(url),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data
        }
        $http(req).then(function(response){ 
            $scope.ajaxCounter--;
            callback(response.data) 
        }, function(err){ 
            $scope.ajaxCounter--;
            if(errorCallback){
                errorCallback(err)
                return;
            }
            else
                Materialize.toast('ERROR: ' + err.status + ' , MESSAGE: ' + err.statusText, 4000);
        });
    }

});
